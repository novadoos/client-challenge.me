'use strict';
// Challenge.me Starter App, v0.1

angular.module('Challengeme', [
    'ionic',
    'config',
    'ionic.utils',
    'ngCookies',
    'ngResource',
    'ngCordova',
    'pascalprecht.translate',
    'ngTagsInput',
    'btford.socket-io'
])
.run(function($ionicPlatform , $rootScope, $state, $urlRouter, $window, $localstorage, $ionicPopup, $translate, ENV, Auth, $cordovaFacebook, $ionicActionSheet, Notification) {
  //Set global class to body if restyling is needed for specific lang
  $rootScope.i18n = $translate.use();
  $rootScope.$on('notification', function (event, data) {
    Notification.notify(data);
  });
  $ionicPlatform.ready(function() {
    window.addEventListener('native.keyboardhide', keyboardHideHandler);

    function keyboardHideHandler(){
      document.activeElement.blur();
    }
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    window.youtube.init('AIzaSyBPDKJmyaz07K3RpG1LAMXnKbUYJ4creTA');
  });

  //App interceptor for MVC control
  $rootScope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams){
      if(toState.data.authenticate && typeof $localstorage.get(ENV.localStorageName) === 'undefined'){
        $state.go("splash");
        event.preventDefault();
      }

      if(toState.name === 'app.create' && $rootScope.activeUpload){
        var alertPopup = $ionicPopup.alert({
          title: 'Wait',
          template: 'Wait until the challenge has finished uploading',
          okType: 'button-energized',
          okText: 'Go back'
        });
        alertPopup.then(function(res) {
          $rootScope.$emit('notReady');
        });
      }
      if(toState.name === 'app.create' || toState.name === 'app.detail'){
        $rootScope.hideTabs = 'tabs-item-hide'
      }
      else{
        $rootScope.hideTabs = ''
      }
      if(toState.name === 'app.profile'){
        Auth.setUser($localstorage.get(ENV.localStorageName));
      }
      if(toState.name == 'splash'){
        $rootScope.inSplash = true;
      }
      else{
        $rootScope.inSplash = false;
      }
  });
})

.config(function($stateProvider, $urlRouterProvider, $httpProvider, $translateProvider,$ionicConfigProvider, $sceDelegateProvider, TRANSLATE) {
    $sceDelegateProvider.resourceUrlWhitelist(['self', new RegExp('^(http[s]?):\/\/(w{3}.)?youtube\.com/.+$')]);
    $ionicConfigProvider.navBar.transition('none');
    $ionicConfigProvider.views.transition('none');
    $ionicConfigProvider.tabs.position('bottom');
    $ionicConfigProvider.backButton.previousTitleText(false).text('');
    $ionicConfigProvider.form.checkbox('circle');
    $ionicConfigProvider.views.maxCache(0);

    $translateProvider.translations('es', TRANSLATE.es);

    $translateProvider.preferredLanguage('en');

    $translateProvider.useLocalStorage();

    $stateProvider

      .state('splash', {
        url: '/splash',
        templateUrl: 'templates/application/splash.html',
        controller: 'ApplicationSplashCtrl',
        data:{
            authenticate: false
        }
      })
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/application/menu.html',
        controller: 'ApplicationMenuCtrl',
        data:{
            authenticate: true
        }
      })
      .state('app.home', {
        url: '/home',
        views: {
          'home' :{
            templateUrl: 'templates/user/app/home/index.html',
            controller: 'UserAppHomeIndexCtrl'
          }
        }
      })
      .state('app.users-home', {
        url: '/users/:id',
        views: {
          'home' :{
            templateUrl: 'templates/user/app/users/index.html',
            controller: 'UserAppUsersIndexCtrl'
          }
        }
      })
      .state('app.challenge-home-tag', {
        url: '/challenges/:tag',
        views: {
          'home' :{
            templateUrl: 'templates/user/app/home/challenge-tag.html'
          }
        }
      })
      .state('app.detail', {
        url: '/detail/:challenge',
        views: {
          'home' :{
            templateUrl: 'templates/user/app/home/detail.html',
            controller: 'ApplicationCommentsCtrl'
          }
        }
      })
      .state('app.search', {
        url: '/search',
        views: {
          'home' :{
            templateUrl: 'templates/user/app/home/search.html'
          }
        }
      })
      .state('app.evidence', {
        url: '/evidence',
        views: {
          'evidence' :{
            templateUrl: 'templates/user/app/evidence/index.html',
            controller: 'UserAppEvidenceIndexCtrl'
          }
        }
      })
      .state('app.users-evidence', {
        url: '/users/:id',
        views: {
          'evidence' :{
            templateUrl: 'templates/user/app/users/index.html',
            controller: 'UserAppUsersIndexCtrl'
          }
        }
      })
      .state('app.create', {
          url: '/create',
          views: {
              'create' :{
                  templateUrl: 'templates/user/app/create/create.html',
                  controller: 'UserAppCreateCreateCtrl'
              }
          }
      })
      .state('app.notifications', {
        url: '/notifications',
        views: {
          'notifications' :{
            templateUrl: 'templates/user/app/notifications/notifications.html',
            controller: 'UserAppNotificationsIndexCtrl'
          }
        }
      })
      .state('app.users-notifications', {
        url: '/users/:id',
        views: {
          'notifications' :{
            templateUrl: 'templates/user/app/users/index.html',
            controller: 'UserAppUsersIndexCtrl'
          }
        }
      })
      .state('app.options', {
        url: '/options',
        views: {
          'profile': {
            templateUrl: 'templates/user/app/profile/options.html',
            controller: 'UserAppProfileOptionsCtrl'
          }
        }
      })
      .state('app.profile', {
        url: '/profile',
        views: {
          'profile' :{
            templateUrl: 'templates/user/app/profile/profile.html'
          }
        }
      })
      .state('app.users-profile', {
        url: '/users/:id',
        views: {
          'profile' :{
            templateUrl: 'templates/user/app/users/index.html',
            controller: 'UserAppUsersIndexCtrl'
          }
        }
      });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise( function($injector, $location) {
    var $state = $injector.get('$state');
    $state.go('app.home');
  });
  //Global HTTP interceptors for global error handling
  $httpProvider.interceptors.push(['$q','$injector','$rootScope','$localstorage','ENV',
    function($q ,$injector ,$rootScope ,$localstorage ,ENV) {
      return {
          request: function (config) {
            return config;
          },
          'responseError': function(response) {
            //IF BACKEND RESPONSE WITH 401 OR 403 THEN REDIRECT TO SPLASH FOR LOGIN
            if(response.status === 401 || response.status === 403) {
              $localstorage.delete(ENV.localStorageName);
              $injector.get('$state').transitionTo('splash');
              return $q.reject(response);
            }
            else {
              return $q.reject(response);
            }
          }
      };
  }]);
});

