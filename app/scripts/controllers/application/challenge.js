'use strict';

angular.module('Challengeme')
  .controller('ApplicationChallengeCtrl', function($scope, $localstorage , $attrs, $anchorScroll, $location, $ionicScrollDelegate, $timeout, $stateParams,  ENV , Challenge) {
    $scope.endpoint = ENV.apiEndpointTrim;
    $scope.media = function(challenge){
      if(challenge.thumbnail){
        return $scope.endpoint + challenge.thumbnail;
      }
      return $scope.endpoint + challenge.media
    };
    $scope.play = function(mediaId){
      window.youtube.playVideo(mediaId , 0 , true, true);
    };
    $scope.resize = function(){
      $ionicScrollDelegate.resize();
    };
    if($attrs.type === 'detail'){
      document.getElementById("replyInput").focus();
    }
    $scope.like = function(challenge){
      Challenge.likeChallenge({challengeId:challenge._id, userId: $localstorage.get(ENV.localStorageName)})
        .then(function(data){
          challenge.voltsCount = data.number;
          if(challenge.hasMyVolt){return challenge.hasMyVolt = false}
          challenge.hasMyVolt = true;
        })
        .catch(function(data){
          challenge.hasMyVolt = false;
        });
    };
    $scope.follow = function(challenge){
      Challenge.followChallenge({id:$localstorage.get(ENV.localStorageName), followToId: challenge._id})
        .then(function(data){
          if(challenge.following){return challenge.following = false}
          challenge.following = true;
        })
        .catch(function(data){
          challenge.following = false;
        });
    };
  });
