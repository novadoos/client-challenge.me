'use strict';

angular.module('Challengeme')
  .controller('ApplicationCommentsCtrl', function($scope , $localstorage , $stateParams,  ENV , Challenge,  CONFIG) {
    $scope.avatarUrl = ENV.apiEndpoint + CONFIG.avatarUrl;
    var myUser = $localstorage.getObject(ENV.localStorageObj);
    $scope.replyCommentBaseInput = {};
    function loadComments() {
      Challenge.getComments({challenge:$stateParams.challenge, index:'0'})
        .then(function(data){
          $scope.comments = data;
        })
        .catch(function(err){
          console.log(err);
        });
    }
    if($stateParams.challenge){
      loadComments();
    }
    $scope.replyComment = {};
    $scope.submitComment = function(form, userId){
      if(form.$valid && $scope.replyComment.comment){
        var replyObj = {
          challengeId: $stateParams.challenge,
          commentMessage: $scope.replyComment.comment,
          userId: myUser._id,
          userUsername: myUser.username,
          parentId: userId
        };
        Challenge.postComment(replyObj)
          .then(function(data){
            console.log(data);
            loadComments();
          })
          .catch(function(err){
            loadComments();
          });
      }
    };
    $scope.submitCommentBase = function(form){
      if(form.$valid && $scope.replyCommentBaseInput.comment){
        Challenge.postComment({
          challengeId: $stateParams.challenge,
          commentMessage: $scope.replyCommentBaseInput.comment,
          userId: myUser._id,
          userUsername: myUser.username
        })
          .then(function(data){
            loadComments();
          })
          .catch(function(err){
            loadComments();
          });
      }

    };
  });
