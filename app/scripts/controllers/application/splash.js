'use strict';

angular.module('Challengeme')
    .controller('ApplicationSplashCtrl', function($scope, $state, $ionicModal, $ionicPopup, Auth, $localstorage,$ionicBackdrop,$cordovaFacebook, ENV, $ionicHistory, CONFIG) {
        $ionicHistory.clearCache();
        $ionicHistory.clearHistory();
        $localstorage.delete(ENV.localStorageName);
        $localstorage.delete(ENV.localStorageObj);
        //Destroy modal of memory
        $scope.$on('$ionicView.leave', function() {
          $scope.signupModal.remove();
          $scope.loginModal.remove();
        });
        //Instance Modals
        $ionicModal.fromTemplateUrl('templates/user/auth/login.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.loginModal = modal;
        });

        $ionicModal.fromTemplateUrl('templates/user/auth/signup.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.signupModal = modal;
        });

        $scope.facebookLogin = function() {
          $ionicBackdrop.retain();
          $cordovaFacebook.login(["public_profile", "email"])
            .then(function() {
              $cordovaFacebook.getAccessToken()
                .then(function(token) {
                  Auth.createFacebookUser({ access_token: token })
                    .then(function(data){
                      $ionicBackdrop.release();
                      $localstorage.set(ENV.localStorageName, data._id);
                      $localstorage.setObject(ENV.localStorageObj, data);
                      $state.go('app.home');
                    })
                    .catch(function(err){
                      $ionicBackdrop.release();
                      $ionicPopup.alert({
                        title: CONFIG.error500Server.title,
                        template: CONFIG.error500Server.msg,
                        okType: 'button-energized'
                      });
                    });
                }, function (error) {
                  $ionicBackdrop.release();
                  console.log(error);
                });
            }, function (error) {
              $ionicBackdrop.release();
              console.log(error);
            });
        };

        $scope.doLogin = function(form){
          if(form.$valid){
            $ionicBackdrop.retain();
            $scope.user = {
              email: form.email.$viewValue,
              password: form.password.$viewValue
            };
            Auth.login($scope.user)
              .then(function(data){
                console.log(data);
                $ionicBackdrop.release();
                $localstorage.set(ENV.localStorageName, data._id);
                $localstorage.setObject(ENV.localStorageObj, data);
                $state.go('app.home');
              })
              .catch(function(err){
                $ionicBackdrop.release();
                if(err.status === 401){
                  $ionicPopup.alert({
                    title: 'Oops',
                    template: 'Looks like your username or password is incorrect',
                    okType: 'button-energized'
                  });
                }
              })
          }
        };

        $scope.doSignup = function(form){
          if(form.$valid){
            $ionicBackdrop.retain();
            $scope.user = {
              username: form.username.$viewValue,
              email: form.email.$viewValue,
              password: form.password.$viewValue
            };
            Auth.createUser($scope.user)
              .then(function(data){
                $ionicBackdrop.release();
                $localstorage.set(ENV.localStorageName, data._id);
                $localstorage.setObject(ENV.localStorageObj, data);
                $state.go('app.home');
              })
              .catch(function(err){
                $ionicBackdrop.release();
                console.log(err);
                if(err.data && err.data.errors && err.data.errors.email){
                  $ionicPopup.alert({
                    title: CONFIG.error500Server.title,
                    template: err.data.errors.email.message,
                    okType: 'button-energized'
                  });
                }else{
                  $ionicPopup.alert({
                    title: CONFIG.error500Server.title,
                    template: CONFIG.error500Server.msg,
                    okType: 'button-energized'
                  });
                }
              });
          }

        }
    });
