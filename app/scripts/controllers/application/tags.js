'use strict';

angular.module('Challengeme')
  .controller('ApplicationChallengeTagCtrl', function($scope, $stateParams, $localstorage, Challenge, ENV, CONFIG) {
    var user = $localstorage.get(ENV.localStorageName);
    var index = 0;
    $scope.avatarUrl = ENV.apiEndpoint + CONFIG.avatarUrl;
    $scope.loadMore = function() {
      loadChallenge();
    };
    function loadChallenge(){
      Challenge.getChallengesByTag({tag:$stateParams.tag,userId:user,skip:index})
        .then(function(data){
          angular.forEach(data, function(value, key) {
            $scope.challenges.push(value);
          });
          if($scope.challenges.length >= 10){
            index += $scope.challenges.length;
          }
        })
        .catch(function(err){
          console.log(err);
        });
    }
    loadChallenge();
  });
