'use strict';

angular.module('Challengeme')
    .controller('UserAppCreateCreateCtrl', function($scope, $state, $rootScope ,$cordovaCapture, $cordovaFile, $ionicModal, ENV, Upload , $localstorage , $cordovaFacebook, Challenge, $ionicBackdrop, $ionicActionSheet, $cordovaCamera, $ionicHistory) {
        function phoneGapSuperFix(mediaURL){
          var parts = mediaURL.split("//");
          var parts1 =  parts[1].split("/");

          if (parts1[0] == "com.android.providers.media.documents"){

            var newMediaURL;
            var endClip = mediaURL.substring(mediaURL.lastIndexOf("/") + 1);
            endClip = decodeURIComponent(endClip);
            endClip = endClip.replace(":","/");
            endClip = endClip.split("/");

            newMediaURL = "content://media/external/video/media/" + endClip[1];

            return newMediaURL;
          } else {
            return mediaURL;
          }
        };
        $scope.user = $localstorage.getObject(ENV.localStorageObj);
        $rootScope.$on('notReady', function(){
          $state.go('app.home');
        });
        var globalshare = {};
        globalshare.shared = false;
        $scope.tag = [];
        $scope.videoGo = false;
        $scope.tagpeople = [];
        $scope.devList = [];
        if(!$rootScope.challenge){
          $rootScope.challenge = {};
        }
        $rootScope.challenge.daysAvailable = 15;
        $scope.videoSrc = false;
        $scope.sheet = function(){
          $ionicActionSheet.show({
            buttons: [
              { text: 'Record' },
              { text: 'Gallery' }
            ],
            titleText: 'Capture video',
            cancelText: 'Cancel',
            cancel: function() {
              $ionicHistory.goBack();
            },
            buttonClicked: function(index) {
              if(index === 0){
                $scope.captureVideo();
              }
              else {
                $scope.captureGallery();
              }
              return true;
            }
          });
        };
        $scope.sheet();
        $ionicModal.fromTemplateUrl('users.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(modal) {
          $scope.users = modal;
        });
        $scope.close = function(){
          $scope.users.remove();
          $state.go('app.home');
        };
        $scope.myGoBack = function() {
          if($scope.videoSrc){
          }
          else{
            $scope.users.remove();
            $state.go('app.home');
          }
        };
        $scope.captureGallery = function(){
          var options = {
            sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: Camera.MediaType.VIDEO
          };

          $cordovaCamera.getPicture(options).then(function(videoData) {
            $scope.videoSrc = phoneGapSuperFix(videoData);
          }, function(err) {
            // TODO: Error handling
          });
        };
        $scope.captureVideo = function(){
          var options = { duration: 15 };
          $cordovaCapture.captureVideo(options).then(function(videoData) {
            $scope.videoSrc = videoData[0].fullPath;
          }, function(err) {

          });
        };
        $scope.playvideo = function(){
          VideoPlayer.play($scope.videoSrc);
        };
        $scope.create = function(){
          if($scope.videoSrc){
            var people = [], tags = [];
            $rootScope.challenge.user = $localstorage.get(ENV.localStorageName);
            angular.forEach($scope.tagpeople, function(valuetag, keytag) {
              people.push(valuetag._id);
            });
            angular.forEach($scope.tag, function(valuetag, keytag) {
              tags.push(valuetag.text);
            });
            $rootScope.challenge.tags = tags;
            $rootScope.challenge.people = people;
            var options = new FileUploadOptions();
            options.fileKey = "file";
            options.httpMethod = "POST";
            options.params = $rootScope.challenge;
            options.headers = {
              Connection: "close"
            };
            if($scope.videoSrc){
              options.fileName = $localstorage.get(ENV.localStorageName);
              options.mimeType = "video/mp4";
              Upload.uploadFile(options, $scope.videoSrc);
            }
            $rootScope.challenge = {};
            $state.go('app.home');
          }
          else{
          }
        };
        $scope.addPerson = function(person){
          delete person.$$hashKey;
          if($scope.tagpeople.length > 0){
            if($scope.tagpeople.indexOf(person) === -1){
              $scope.tagpeople.push(person);
            }
            else{
              if(!person.checked){
                $scope.tagpeople.splice($scope.tagpeople.indexOf(person) , 1);
              }
            }
          }
          else{
            if(person.checked){
              $scope.tagpeople.push(person);
            }
          }
        };
        $scope.getUsers = function(val) {
          if(val && val !== '' && val.length > 2){
            Challenge.getUsers({val:val,username:$scope.user.username})
              .then(function(response){
                if(response.length){
                  angular.forEach(response, function(value, key) {
                    angular.forEach($scope.tagpeople, function(valuetag, keytag) {
                      if(value._id === valuetag._id){
                        value.checked = true;
                      }
                    });
                  });
                  $scope.devList = response;
                }
                else{
                  $scope.devList = $scope.tagpeople || [];
                }
              })
              .catch(function(err){
                $scope.devList = $scope.tagpeople || [];
              });
          }
          else{
            $scope.devList = $scope.tagpeople || [];
          }
        };
        $scope.share = function(shared){
          $ionicBackdrop.retain();
          globalshare = shared;
          if(globalshare.shared){
            $cordovaFacebook.login(["public_profile", "email"])
              .then(function(success) {
                $cordovaFacebook.getAccessToken()
                  .then(function(token) {
                    if(token){
                      $ionicBackdrop.release();
                      $rootScope.challenge.fbToken = token;
                      $rootScope.challenge.shared = true;
                    }
                    else{
                      $ionicBackdrop.release();
                      globalshare.shared = false;
                      delete $rootScope.challenge.shared;
                      $rootScope.challenge.shared = false;
                    }
                  }, function (error) {
                    $ionicBackdrop.release();
                    globalshare.shared = false;
                    delete $rootScope.challenge.shared;
                    $rootScope.challenge.shared = false;
                  });
              }, function (error) {
                $ionicBackdrop.release();
                globalshare.shared = false;
                delete $rootScope.challenge.shared;
                $rootScope.challenge.shared = false;
              });
          }
          else{
            $ionicBackdrop.release();
            globalshare.shared = false;
            delete $rootScope.challenge.shared;
          }
          return shared.shared = false;
        }

    });
