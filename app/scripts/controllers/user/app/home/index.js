'use strict';

angular.module('Challengeme')
    .controller('UserAppHomeIndexCtrl', function($scope, $state , $localstorage, CONFIG, Challenge, ENV) {
        var user = $localstorage.get(ENV.localStorageName);
        var index = 0;
        $scope.avatarUrl = ENV.apiEndpoint + CONFIG.avatarUrl;
        $scope.goUser = function(id){
          $state.go('app.users-home' , { id: id });
        };
        $scope.goTag = function(tag){
          $state.go('app.challenge-home-tag' , { tag: tag });
        };
        $scope.goDetail = function(id){
          $state.go('app.detail' , { challenge: id });
        };
        $scope.challenges = [];
        $scope.loadMore = function() {
          loadChallenge();
        };
        function loadChallenge(){
          Challenge.getChallenges({user:user, index:index})
            .then(function(data){
              angular.forEach(data, function(value, key) {
                $scope.challenges.push(value);
              });
              if($scope.challenges.length >= 10){
                index += $scope.challenges.length;
              }
            })
            .catch(function(err){
              console.log(err);
            });
        }
        loadChallenge();

        $scope.go = function(){
            $state.go('app.notifications');
        };
    });
