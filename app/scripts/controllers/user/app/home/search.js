'use strict';

angular.module('Challengeme')
  .controller('UserAppSearchIndexCtrl', function($scope, $state, ENV) {
    $scope.items = [];
    $scope.goBack = function(){
      $state.go('app.home');
    };

    $scope.getItemHeight = function(item, index) {
      return (index % 2) === 0 ? 50 : 60;
    };
    $scope.follow = function(user){
      console.log(user);
    };
    $scope.clearSearch = function() {
      $scope.search = '';
      $scope.items = [];
    };
});
