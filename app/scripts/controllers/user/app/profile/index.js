'use strict';

angular.module('Challengeme')
  .controller('UserAppProfileIndexCtrl', function($scope, $state, $location, $ionicScrollDelegate, $localstorage, ENV, $stateParams , $ionicModal , Auth, UserAvatar, Challenge) {
    $scope.created = true;
    function setChallenges(){
      $scope.created = false;
      $scope.progress = false;
      $scope.completed = false;
    }
    $scope.canFollow = function(){
      return $stateParams.id !== void 0;
    };
    $scope.follow = function(){
      Auth.followUser({id:$localstorage.getObject(ENV.localStorageObj)._id, followToId: $stateParams.id})
        .then(function(data){
          console.log(data);
        })
        .catch(function(err){
          console.log(err);
        });
    };
    $scope.created = true;
    if($stateParams.id && $stateParams.id !== '' && typeof $stateParams.id !== 'undefined'){
      Auth.getUser($stateParams.id)
        .then(function(data){
          $scope.user = data;
          console.log($scope.user);
          $scope.user.picture = UserAvatar.url($stateParams.id);
          if(!$scope.user.username && $scope.user.name)
            $scope.user.username = $scope.user.name;
        })
        .catch(function(err){
          console.log(err);
        });
      Challenge.getChallengesByUserId({userId:$stateParams.id,skip:0})
        .then(function(data){
          $scope.challenges = data;
        })
        .catch(function(data){
          console.log(data);
        });
    }
    else{
      $scope.user = $localstorage.getObject(ENV.localStorageObj);
      $scope.user.isMe = true;
      $scope.user.picture = UserAvatar.url();
      if(!$scope.user.username && $scope.user.name)
        $scope.user.username = $scope.user.name;
      $scope.avatarUrl = ENV.apiEndpoint + CONFIG.avatarUrl;
      var userId = $localstorage.get(ENV.localStorageName);
      Challenge.getChallengesByUserId({userId:userId,skip:0})
        .then(function(data){
          $scope.challenges = data;
        })
        .catch(function(data){
          console.log(data);
        });
    }

    $scope.evidence = function() {
      $ionicModal.fromTemplateUrl('evidence.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.evidenceModal = modal;
        $scope.evidenceModal.show();
      });
    };

    $scope.tabChallenge = true;
    $scope.setChallenge = function(num){
      switch (num) {
        case 1:
          setChallenges();
          $scope.created = true;
          break;
        case 2:
          setChallenges();
          $scope.progress = true;
          break;
        case 3:
          setChallenges();
          $scope.completed = true;
          break;
      }
      $ionicScrollDelegate.resize();
    };

    $scope.tabs = function(num){
      switch (num) {
        case 0:
          $scope.tabChallenge = true;
          $scope.tabFollowers = false;
          $scope.tabFollowing = false;
          break;
        case 1:
          $scope.tabChallenge = false;
          $scope.tabFollowers = true;
          $scope.tabFollowing = false;
          break;
        case 2:
          $scope.tabChallenge = false;
          $scope.tabFollowers = false;
          $scope.tabFollowing = true;
          break;
      }
      $ionicScrollDelegate.resize();
    }

  });
