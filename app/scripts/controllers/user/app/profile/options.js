'use strict';

angular.module('Challengeme')
  .controller('UserAppProfileOptionsCtrl', function($scope,$state,$localstorage,$ionicModal,$translate,$cordovaCamera,Auth,ENV, UserAvatar) {
    $scope.password = {};

    $scope.profileImg = UserAvatar.url();

    $ionicModal.fromTemplateUrl('templates/directives/user/upload-picture.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.uploadModal = modal;
    });

    $ionicModal.fromTemplateUrl('templates/directives/user/upload-evidence.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.evidenceModal = modal;
    });

    $ionicModal.fromTemplateUrl('change-lang.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modalLang = modal;
      $scope.lang = $translate.use();
    });

    $ionicModal.fromTemplateUrl('templates/directives/user/change-password.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.changepass = modal;
    });

    //Destroy modals of memory
    $scope.$on('$ionicView.leave', function(e) {
      $scope.changepass.remove();
      $scope.modalLang.remove();
      $scope.uploadModal.remove();
      $scope.evidenceModal.remove();
    });

    $scope.logout = function(){
      $localstorage.delete(ENV.localStorageName);
      $state.go('splash');
    };

    $scope.capturePhoto = function(){
      var options = {
        quality : 75,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : Camera.PictureSourceType.CAMERA,
        allowEdit : false,
        encodingType: Camera.EncodingType.JPEG,
        saveToPhotoAlbum: false,
        targetWidth: 200,
        targetHeight: 200
      };

      $cordovaCamera.getPicture(options).then(function(imageData) {
        var imgdata = "data:image/jpeg;base64," + imageData;
        Auth.setPicture(imgdata)
          .then(function(data){
            $scope.profileImg = UserAvatar.url();
          })
          .catch(function(err){
            // TODO: Error handling
            console.log(err);
          });
      }, function(err) {
         // TODO: Error handling
      });
    };
    $scope.changeLang = function(lang){
      $translate.use(lang)
    };
    $scope.changePass = function(form){
      if(form.$valid && $scope.password && $scope.password.new === $scope.password.renew){

      }
    };
    $scope.captureGallery = function(){
      var options = {
        quality : 75,
        destinationType : Camera.DestinationType.DATA_URL,
        sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
        allowEdit : false,
        encodingType: Camera.EncodingType.JPEG,
        targetWidth: 200,
        targetHeight: 200
      };

      $cordovaCamera.getPicture(options).then(function(imageData) {
        var imgdata = "data:image/jpeg;base64," + imageData;
        Auth.setPicture(imgdata)
          .then(function(data){
            $scope.profileImg = UserAvatar.url();
          })
          .catch(function(err){
            // TODO: Error handling
            console.log(err);
          });
      }, function(err) {
          // TODO: Error handling
      });
    }

  });
