'use strict';

angular.module('Challengeme')
  .directive('challengeDirective', function() {
    return {
      templateUrl: function(elem, attr){
        return 'templates/directives/challenge/challenge-'+attr.type+'.html';
      },
      controller: 'ApplicationChallengeCtrl'
    };
  });
