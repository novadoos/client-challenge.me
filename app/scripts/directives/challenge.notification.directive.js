'use strict';

angular.module('Challengeme')
  .directive('challengeNotificationDirective', function() {
    return {
      templateUrl: function(elem, attr){
        return 'templates/directives/user/notification.html';
      },
      controller:function($scope, $rootScope){
        $scope.closeComplete = function(){
          delete $rootScope.notification.show;
        }
      }

    };
  });
