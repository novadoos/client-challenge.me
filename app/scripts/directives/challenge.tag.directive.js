'use strict';

angular.module('Challengeme')
  .directive('tagDirective', function() {
    return {
      template: '<ion-content class="has-header padding home tags"><div challenge-directive type="general"></div></ion-content>',
      controller: 'ApplicationChallengeTagCtrl'
    };
  });
