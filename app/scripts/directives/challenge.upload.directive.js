'use strict';

angular.module('Challengeme')
  .directive('challengeUploadDirective', function() {
    return {
      templateUrl: function(elem, attr){
        return 'templates/directives/create/upload.html';
      }

    };
  });
