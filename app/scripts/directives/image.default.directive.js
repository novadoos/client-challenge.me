'use strict';

angular.module('Challengeme')
  .directive('errSrc', function(ENV, CONFIG) {
    var avatarUrl = ENV.apiEndpoint + CONFIG.avatarUrl + 'default.gif';
    return {
      link: function(scope, element, attrs) {
        element.bind('error', function() {
          if(attrs.src !== avatarUrl){
            attrs.$set('src', avatarUrl);
          }
        });
      }
    }
  });
