'use strict';

angular.module('Challengeme')
  .directive('userProfileDirective', function() {
    return {
      restrict: 'E',
      templateUrl: 'templates/directives/user/profile.html',
      controller:'UserAppProfileIndexCtrl'
    };
  });
