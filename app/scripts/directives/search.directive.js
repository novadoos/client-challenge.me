'use strict';

angular.module('Challengeme')
  .directive('searchDirective', function() {
    return {
      restrict: 'E',
      templateUrl: 'templates/directives/search/search.html',
      controller:'UserAppSearchIndexCtrl'
    };
  });
