'use strict';

angular.module('Challengeme')
  .constant("CONFIG", {
    error500Server: {
      title:'Woa',
      msg:'Sorry but our service is not avaiable right now, please try again later'
    },
    success200Server: {
      title:'Nice',
      msg:'It looks good'
    },
    avatarUrl: 'public/users/avatar/'
  })
  .constant("TRANSLATE", {
    es:{
      'Facebook Sign in': 'Iniciar sesión con Facebook',
      'Email Sign Up': 'Crear cuenta',
      'Log In': 'Iniciar sesión',
      'Profile picture':'Foto del perfil',
      'Gallery':'Galería',
      'Challenges':'Retos',
      'Followers':'Seguidores',
      'Following':'Siguiendo',
      'Completed':'Realizado',
      'In progress':'En curso',
      'Account':'Cuenta',
      'Upload profile picture':'Subir foto de perfil',
      'Log out':'Cerrar sesión',
      'More':'Mas',
      'Create':'Crear',
      'World':'Mundial',
      'Direct':'Directo',
      'Name the challenge':'Nombra tu reto',
      'Photo':'Foto',
      'Description':'Descripción',
      'How long util?':'¿Hasta cuando?',
      'Days':'Días',
      'Drag for how long will the challenge be available in days':'Arrastre por cuánto tiempo será el reto disponible en días',
      'Share':'Compartir',
      'challenged':'ha retado',
      'Take Challenge':'Tomar Reto',
      'View more':'Ver más',
      'Change language':'Cambiar el idioma',
      'Notifications':'Notificaciones',
      'Seek Challenges':'Buscar Retos',
      'Hot':'Caliente',
      'Trending':'Tendencia',
      'Write a comment':'Escribir un comentario',
      'Accept':'Aceptar',
      'Deny':'Negar',
      'Sign up':'Crear cuenta',
      'Username':'Usuario',
      'Password':'Contraseña',
      'Email':'Correo electrónico',
      'Log in':'Iniciar sesión',
      'Please type you email and password':'Por favor, ingrese el correo electrónico y contraseña',
      'Change password':'Cambiar la contraseña',
      'Current password':'Contraseña actual',
      'New password':'Nueva contraseña',
      'Re-enter new password':'Reingresar contraseña nueva',
      'Profile':'Perfil',
      'Upload Media':'Subir multimedia',
      'Tags':'Etiquetas',
      'Add a tag':'Agregar etiqueta',
      'Interests':'Interés',
      'Create Challenge':'Crear Reto'
    }
  })
;
