'use strict';

angular.module('Challengeme')
  .factory('Notification', function Upload($rootScope) {
    $rootScope.notification = {};
    return {
      notify: function(data) {
        $rootScope.notification.show = 'done';
      }
    };
  });
