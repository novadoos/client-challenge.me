'use strict';

angular.module('Challengeme')
  .factory('Upload', function Upload($rootScope, $timeout, $interval, $cordovaFileTransfer, ENV) {
    return {
      uploadFile: function(options, file) {
        $rootScope.loadPercent = 0;

        $rootScope.activeUpload = true;

        $rootScope.closeComplete = function(){
          delete $rootScope.completeUpload;
          $timeout(function() {
            delete $rootScope.activeUpload;
          }, 1000);
        };

        $cordovaFileTransfer.upload(ENV.apiEndpoint + 'api/challenges', file, options)
          .then(function(result) {
            console.log(result);
            $rootScope.completeUpload = 'done';
            delete $rootScope.loadPercent;
            $rootScope.$broadcast('getChallenges:broadcast');
          }, function(err) {
            delete $rootScope.loadPercent;
            delete $rootScope.activeUpload;
            delete $rootScope.completeUpload;
          }, function (progress) {
            $rootScope.loadPercent = Math.floor(progress.loaded / progress.total * 100);
          });
      }
    };
  });
