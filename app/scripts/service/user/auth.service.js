'use strict';

angular.module('Challengeme')
  .factory('Auth', function Auth($rootScope, User, FacebookUser, UserPicture, UserLogin, $localstorage, UserFollow, ENV) {
    var userId = $localstorage.get(ENV.localStorageName);

    return {
      createUser: function(user, callback) {
        var cb = callback || angular.noop;

        return User.save(user,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      login: function(user, callback) {
        var cb = callback || angular.noop;

        return UserLogin.get(user,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      createFacebookUser: function(user, callback) {
        var cb = callback || angular.noop;

        return FacebookUser.save(user,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      getUser: function(user, callback) {
        var cb = callback || angular.noop;
        return User.get({id:user},
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      followUser: function(user, callback) {
        var cb = callback || angular.noop;
        return UserFollow.save({id:user.id, followToId: user.followToId},
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      setPicture: function(imgdata, callback) {
        var cb = callback || angular.noop;
        return UserPicture.save({imgdata:imgdata,userId:userId},
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      setUser:function(id){
        userId = id;
      }
    };
  });
