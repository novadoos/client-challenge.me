'use strict';

angular.module('Challengeme')
  .factory('Challenge', function Auth(ChallengeGet, ChallengeLike, ChallengeFollow, CommentsGet, CommentsPost, UsersGet, ChallengeGetByTag, ChallengesByUserId) {

    return {
      getChallenges: function(data, callback) {
        var cb = callback || angular.noop;

        return ChallengeGet.get(data,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      getChallengesByTag: function(data, callback) {
        var cb = callback || angular.noop;

        return ChallengeGetByTag.get(data,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      getChallengesByUserId: function(data, callback) {
        var cb = callback || angular.noop;

        return ChallengesByUserId.get(data,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      getComments: function(data, callback) {
        var cb = callback || angular.noop;

        return CommentsGet.get(data,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      getUsers: function(data, callback) {
        var cb = callback || angular.noop;

        return UsersGet.get(data,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      postComment: function(data, callback) {
        var cb = callback || angular.noop;

        return CommentsPost.save(data,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      likeChallenge: function(data, callback) {
        var cb = callback || angular.noop;

        return ChallengeLike.save(data,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      },
      followChallenge: function(data, callback) {
        var cb = callback || angular.noop;

        return ChallengeFollow.save(data,
          function(data) {
            return cb(data);
          },
          function(err) {
            return cb(err);
          }.bind(this)).$promise;
      }
    };
  });
