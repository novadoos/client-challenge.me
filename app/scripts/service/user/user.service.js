'use strict';

angular.module('Challengeme')
  .factory('User', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint + 'api/users/:id', {id: '@id'},
      {
        'update': { method:'PUT' }
      });
  })
  .factory('FacebookUser', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint + 'api/users/facebook')
  })
  .factory('UserFollow', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint + 'api/users/setUserFollower', {} , {
      save:{
        method: 'POST',
        isArray: true
      }
    })
  })
  .factory('UserLogin', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint + 'api/users/login/:email/:password')
  })
  .factory('UserPicture', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint + 'api/users/avatar')
  })
  .factory('ChallengeGet', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint +'api/challenges/getWorld/:user/:index' , {} , {
      get:{
        isArray: true
      }
    })
  })
  .factory('ChallengeGetByTag', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint +'api/challenges/getAllByTag/:tag/:userId/:skip' , {} , {
      get:{
        isArray: true
      }
    })
  })
  .factory('ChallengesByUserId', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint +'api/challenges/getAllById/:userId/:skip' , {} , {
      get:{
        isArray: true
      }
    })
  })
  .factory('CommentsGet', function ($resource, ENV) {
    return $resource(ENV.apiEndpoint +'api/comments/getAll/:challenge/:index' , {} , {
      get:{
        isArray: true
      }
    })
  })
  .factory('UsersGet', function ($resource, ENV) {
    return $resource(ENV.apiEndpoint +'api/users/lists/:val/:username' , {} , {
      get:{
        isArray: true
      }
    })
  })
  .factory('CommentsPost', function ($resource, ENV) {
    return $resource(ENV.apiEndpoint +'api/comments/')
  })
  .factory('ChallengeLike', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint + 'api/challenges/like')
  })
  .factory('ChallengeFollow', function ($resource,ENV) {
    return $resource(ENV.apiEndpoint + 'api/users/setChallengeFollower', {} , {
      save:{
        method: 'POST',
        isArray: true
      }
    })
  })
  .factory('UserAvatar', function (CONFIG,ENV,$localstorage) {
    return {
      url: function(id){
        if(id){return ENV.apiEndpoint + CONFIG.avatarUrl + id + '.jpeg?id='+ Math.floor((Math.random() * 100) + 1)}
        return ENV.apiEndpoint + CONFIG.avatarUrl + $localstorage.get(ENV.localStorageName) + '.jpeg?id='+ Math.floor((Math.random() * 100) + 1)
      }
    };
  });
